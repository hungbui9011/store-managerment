import json
from odoo import http, _
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal, pager
from odoo.tools import groupby as groupbyelem
from operator import itemgetter


class Hospital(http.Controller):

    @http.route(['/hospital'], website=True, auth="public")
    def hospital_patient(self):
        return request.render("om_hospital.hospital_page")

    @http.route(['/hospital/patient'], website=True, auth="public")
    def hospital_patient(self):
        patient = request.env['hospital.patient'].sudo().search([])
        return request.render("om_hospital.patients_page", {'patient': patient})

    @http.route('/hospital/patient/<model("hospital.patient"):patient>', auth='public', website=True)
    def display_patient(self, patient):
        return http.request.render('om_hospital.patients_details', {'patient': patient})

    @http.route(['/hospital/appointment'], auth='public', website=True)
    def hospital_appointment(self):
        appointment = request.env['hospital.appointment'].sudo().search([])
        return http.request.render('om_hospital.appointment_page', {'appointment': appointment})

    @http.route(['/hospital/doctor'], auth='public', website=True)
    def hospital_doctor(self):
        doctor = request.env['hospital.doctor'].sudo().search([])
        return http.request.render('om_hospital.doctor_page', {'doctor': doctor})

    @http.route('/hospital/doctor/<model("hospital.doctor"):doctor>', auth='public', website=True)
    def display_doctor(self, doctor):
        return http.request.render('om_hospital.doctor_details', {'doctor': doctor})


class CustomerPortalTest(CustomerPortal):
    def _prepare_home_portal_values(self, counters):
        rtn = super(CustomerPortalTest, self)._prepare_home_portal_values(counters)
        if 'patient_count' in counters:
            rtn['patient_count'] = request.env['hospital.patient'].search_count([])
        if 'patient_register' in counters:
            rtn['patient_register'] = request.env['hospital.patient'].search_count([])
        return rtn

    @http.route(['/new/patient'], auth='user', methods=["POST", "GET"], website=True)
    def new_patient(self, **kw):
        patient_list = request.env['hospital.patient'].search([])
        vals = {'patient': patient_list, 'page_name': 'new_patient'}
        print(kw)
        if request.httprequest.method == "POST":
            name = kw.get("name")
            age = kw.get("age")
            phone = kw.get("phone")
            email = kw.get("email")
            note = kw.get("note")
            error_list = []
            if not kw.get("name"):
                error_list.append("Name fields is empty")
            if not kw.get("age"):
                error_list.append("Age fields is empty")
            if not kw.get("phone"):
                error_list.append("Phone fields is empty")
            if not kw.get("email"):
                error_list.append("Email fields is empty")
            if not error_list:
                request.env['hospital.patient'].sudo().create(
                    {"name": name, "age": age, 'phone': phone, 'email': email,
                     "note": note or 'New patient'})
                success = "Success create new patient"
                vals['success_msg'] = success
            else:
                vals['error_list'] = error_list

        return http.request.render("om_hospital.new_patient", vals)

    @http.route(['/my/patient', '/my/patient/page/<int:page>'], auth='public', website=True)
    def PatientListView(self, page=1, sortby='age', search="", search_in="All", groupby="none", **kw):
        searchbar_sortings = {
            'reference': {'label': _('Reference'), 'order': 'reference'},
            'name': {'label': _('Name'), 'order': 'name asc'},
            'age': {'label': _('Age'), 'order': 'age'},
        }
        search_list = {
            'All': {'label': _('All'), 'input': 'All', 'domain': [('name', 'ilike', search)]},
            'Name': {'label': _('Name'), 'input': 'Name', 'domain': [('name', 'ilike', search)]},
            'Gender': {'label': _('Gender'), 'input': 'Gender', 'domain': ('gender', 'ilike', search)},
        }
        searchbar_groupby = {
            'none': {'input': 'none', 'label': 'None', 'order': 1},
            'gender': {'input': 'gender', 'label': 'Gender', 'order': 1},
            'state': {'input': 'state', 'label': 'State', 'order': 1},
        }
        patient_group_by = searchbar_groupby.get(groupby, {})
        search_domain = search_list[search_in]['domain']
        order = searchbar_sortings[sortby]['order']
        if groupby in ("gender", "state"):
            patient_group_by = patient_group_by.get("input")
            order = patient_group_by + "," + order
        else:
            patient_group_by = ''
        patient = request.env['hospital.patient']
        total_patient = patient.search_count(search_domain)
        page_details = pager(url='/my/patient',
                             total=total_patient,
                             page=page,
                             url_args={'sortby': sortby,
                                       'search_in': search_in,
                                       'search': search,
                                       'groupby': groupby},
                             step=10)
        patients = patient.search(search_domain, limit=10, order=order, offset=page_details['offset'])
        if patient_group_by:
            patient_group_list = [{patient_group_by: k, 'patients': patient.concat(*g)}
                                  for k, g in groupbyelem(patients, itemgetter(patient_group_by))]
        else:
            patient_group_list = [{'patients': patients}]

        print(patient_group_list)
        vals = {
            'group_patient': patient_group_list,
            # 'patient': patients,
            'page_name': 'patient_list', 'pager': page_details,
            'searchbar_sortings': searchbar_sortings,
            'default_url': '/my/patient',
            'sortby': sortby,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_groupby': searchbar_groupby,
            'groupby': groupby,
            'search_in': search_in,
            'searchbar_inputs': search_list,
            'search': search
        }
        return request.render("om_hospital.patient_list_view", vals)

    @http.route(['/my/patient/<model("hospital.patient"):patient>'], auth='public', website=True)
    def PatientDetailsView(self, patient):
        vals = {'patient': patient, 'page_name': 'patient_form_view'}
        patient_records = request.env['hospital.patient'].search([])
        patient_ids = patient_records.ids
        patient_index = patient_ids.index(patient.id)
        if patient_index != 0 and patient_ids[patient_index - 1]:
            vals['pre_record'] = '/my/patient/{}'.format(patient_ids[patient_index - 1])
        if patient_index < len(patient_ids) - 1 and patient_ids[patient_index + 1]:
            vals['next_record'] = '/my/patient/{}'.format(patient_ids[patient_index + 1])
        return request.render("om_hospital.patient_form_view", vals)

    @http.route(['/my/patient/print/<model("hospital.patient"):patient>'], auth='public', website=True)
    def PatientReport(self, patient, **kw):
        return self._show_report(model=patient, report_type='pdf', report_ref='om_hospial.report_all_patient_list',
                                 download=True)
