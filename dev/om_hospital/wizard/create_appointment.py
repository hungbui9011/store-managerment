from odoo import api, fields, models, _


class CreateAppointmentWizard(models.TransientModel):
    _name = "create.appointment.wizard"
    _description = "Create Appointment Wizard"

    @api.model
    def default_get(self, fields):
        res = super(CreateAppointmentWizard, self).default_get(fields)
        res['patient_id'] = self._context.get('active_id')
        return res

    date_appointment = fields.Date(string='Date', requied=True)
    patient_id = fields.Many2one('hospital.patient', string="Patient", required=True)
    doctor_id = fields.Many2one('hospital.doctor', string="Doctor", required=True)

    def create_appointment_wizard(self):
        vals = {
            'patient_id': self.patient_id.id,
            'date_appointment': self.date_appointment,
            'doctor_id': self.doctor_id.id
        }
        appointment_rec = self.env['hospital.appointment'].create(vals)
        return {
            'name': _('Appointment'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'hospital.appointment',
            'res_id': appointment_rec.id,
            'target': 'new'
        }


    def view_appointment_wizard(self):
        # method 1
        view = self.env.ref('om_hospital.action_hospital_appointment').read()[0]
        view['domain'] = [('patient_id', '=', self.patient_id.id)]
        return view

        # # method 2
        # action = self.env['ir.actions.actions']._for_xml_id("om_hospital.action_hospital_appointment")
        # action['domain']=[('patient_id','=',self.patient_id.id)]

        # # # method 3
        # return {
        #     'name': 'Appointment',
        #     'type': 'ir_actions.act_window',
        #     'view_mode': 'tree,form',
        #     'view_type': 'form',
        #     'res_model': 'hospital.appointment',
        #     'domain':[('patient_id','=',self.patient_id.id)]
        #     'target': 'current',
        # }