from odoo import fields, models, api
from odoo.sql_db import db_connect


class SaleReport(models.TransientModel):
    _name = 'report.sale.wizard'
    _description = 'Print Sale Report Wizard'

    def action_print_ex_report(self):
        sale = f"""
                    WITH tra_hang AS (
                        SELECT SUBSTRING
                            ( sp.origin, 11 ) AS origin,
                            pt.ID,
                            sm.product_uom_qty AS dang_tra,
                            sm.quantity_done AS da_tra 
                        FROM
                            stock_move_line sml
                            LEFT JOIN stock_move sm ON sml.move_id = sm."id"
                            LEFT JOIN stock_picking sp ON sml.picking_id = sp."id"
                            LEFT JOIN res_partner rp ON sm.partner_id = rp."id"
                            LEFT JOIN product_product pp ON sm.product_id = pp."id"
                            LEFT JOIN product_template pt ON pp.product_tmpl_id = pt."id"
                            LEFT JOIN stock_location ds ON sm.location_id = ds."id" 
                        WHERE
                            ds."usage" = 'customer' 
                        ) SELECT
                        sml.reference,
                        sp.origin,
                        rp."name",
                        pt.NAME [ 'en_US' ] AS tensp,
                        sol.product_uom_qty AS demand,
                        CASE WHEN sol.qty_delivered > 0 THEN sol.qty_delivered ELSE 0 END as da_giao,
                        CASE WHEN th.dang_tra > 0 THEN th.dang_tra ELSE 0 END as dang_tra,
                        CASE WHEN th.da_tra > 0 THEN th.da_tra ELSE 0 END as da_tra,
                        sol.price_unit AS gia_ban,
                        sol.purchase_price AS gia_nhap,
                        CASE WHEN sol.margin > 0 THEN sol.margin ELSE 0 END as tien_lai, 
                        ABS(CASE WHEN sol.margin < 0 THEN sol.margin ELSE 0 END) as tien_lo, 
                        to_char(sml.date, 'DD-MM-YYYY') AS Ngay_thang
                    FROM
                        stock_move_line sml
                        LEFT JOIN stock_move sm ON sml.move_id = sm."id"
                        LEFT JOIN res_partner rp ON sm.partner_id = rp."id"
                        LEFT JOIN stock_location ds ON sml.location_dest_id = ds."id"
                        LEFT JOIN product_product pp ON sml.product_id = pp."id"
                        LEFT JOIN product_template pt ON pp.product_tmpl_id = pt."id"
                        LEFT JOIN tra_hang th ON th.origin = sml.reference
                        LEFT JOIN stock_picking sp ON sml.picking_id = sp."id"
                        INNER JOIN sale_order so ON sp.sale_id = so."id"
                        LEFT JOIN sale_order_line sol ON so.ID = sol.order_id 
                        AND sml.product_id = sol.product_id 
                    WHERE
                        ds.USAGE = 'customer' 
                        AND sml.STATE = 'done'
                    ORDER BY
                        sp.origin ASC;
                    """
        self.env.cr.execute(sale)
        sl = self.env.cr.fetchall()
        print(sl)
        data = {
            'sale': sl
        }
        return self.env.ref('om_hospital.report_sale_xlsx').report_action(self, data=data)
