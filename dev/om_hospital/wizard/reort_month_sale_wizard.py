from odoo import fields, models, api
from odoo.exceptions import ValidationError



class SaleMonthReport(models.Model):
    _name = 'report.month.sale.wizard'
    _description = 'Print Month Sale Report Wizard'

    date_from = fields.Date(string="Date From")
    date_to = fields.Date(string="Date To")

    @api.constrains('date_from', 'date_to','date_time')
    def _check_not_null_date_from(self):
        for record in self:
            if not record.date_from:
                raise ValidationError("Thiếu giá trị")
            if not record.date_to:
                raise ValidationError("Thiếu giá trị")
            if record.date_to > fields.Date.today():
                raise ValidationError("Nhập sai giá trị ngày kết thúc")

    def action_print_bc_report(self):
        sale = f"""
            WITH tra_hang AS (
                SELECT SUBSTRING
                    ( sp.origin, 11 ) AS origin,
                    pt.ID,
                    sm.product_uom_qty AS dang_tra,
                    sm.quantity_done AS da_tra 
                FROM
                    stock_move_line sml
                    LEFT JOIN stock_move sm ON sml.move_id = sm."id"
                    LEFT JOIN stock_picking sp ON sml.picking_id = sp."id"
                    LEFT JOIN res_partner rp ON sm.partner_id = rp."id"
                    LEFT JOIN product_product pp ON sm.product_id = pp."id"
                    LEFT JOIN product_template pt ON pp.product_tmpl_id = pt."id"
                    LEFT JOIN stock_location ds ON sm.location_id = ds."id" 
                WHERE
                    ds."usage" = 'customer' 
            ) SELECT
            sml.reference,
            sp.origin,
            rp."name",
            pt.name['en_US'] AS tensp,
            sol.product_uom_qty AS demand,
            CASE WHEN sol.qty_delivered > 0 THEN sol.qty_delivered ELSE 0 END as da_giao,
            CASE WHEN th.dang_tra > 0 THEN th.dang_tra ELSE 0 END as dang_tra,
            CASE WHEN th.da_tra > 0 THEN th.da_tra ELSE 0 END as da_tra,
            sol.price_unit AS gia_ban,
            sol.purchase_price AS gia_nhap,
            CASE WHEN sol.margin > 0 THEN sol.margin ELSE 0 END as tien_lai, 
            ABS(CASE WHEN sol.margin < 0 THEN sol.margin ELSE 0 END) as tien_lo, 
            sml.date
            FROM
            stock_move_line sml
            LEFT JOIN stock_move sm ON sml.move_id = sm."id"
            LEFT JOIN res_partner rp ON sm.partner_id = rp."id"
            LEFT JOIN stock_location ds ON sml.location_dest_id = ds."id"
            LEFT JOIN product_product pp ON sml.product_id = pp."id"
            LEFT JOIN product_template pt ON pp.product_tmpl_id = pt."id"
            LEFT JOIN tra_hang th ON th.origin = sml.reference
            LEFT JOIN stock_picking sp ON sml.picking_id = sp."id"
            INNER JOIN sale_order so ON sp.sale_id = so."id"
            LEFT JOIN sale_order_line sol ON so.ID = sol.order_id 
            AND sml.product_id = sol.product_id 
            WHERE
            ds.USAGE = 'customer' 
            AND sml.STATE = 'done'
            AND sml.date BETWEEN '{self.date_from}' AND '{self.date_to}'
            ORDER BY
            sp.origin ASC;
        """
        self.env.cr.execute(sale)
        sl = self.env.cr.fetchall()
        data = {
            'sale': sl
        }
        return self.env.ref('om_hospital.report_month_sale_xlsx').report_action(self, data=data)

    def action_print_lk_report(self):
        luy_ke = f"""
                WITH So_du_dau_ki as (
                SELECT
                    sml.product_id,
                    pt.name ['en_US']AS tensp,
                    SUM(CASE WHEN ls.usage = 'internal' AND ds.usage != 'internal' THEN -sml.qty_done
                    WHEN ls.usage != 'internal' AND ds.usage = 'internal' THEN sml.qty_done ELSE 0 END) as sdck
                FROM
                    stock_move_line sml
                    LEFT JOIN product_product pp ON sml.product_id = pp.id
                    LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id  
                    LEFT JOIN stock_location ls ON sml.location_id = ls.id
                    LEFT JOIN stock_location ds ON sml.location_dest_id = ds.id
                WHERE sml.date < '{self.date_from}'
                GROUP BY
                        sml.product_id,
                        pp.id,
                    pt.name
                )
                SELECT
                    pt.name ['en_US']AS tensp,
                    SUM(CASE WHEN ds.usage = 'internal' THEN sml.qty_done ELSE 0 END) AS ths_sln,
                    SUM(CASE WHEN ds.usage != 'internal' THEN sml.qty_done ELSE 0 END) AS ths_slx,
                    CASE WHEN lmt_count.sdck is not NULL THEN lmt_count.sdck  ELSE 0 END,
                    CASE WHEN lmt_count.sdck != 0 THEN
                    ABS(SUM(CASE WHEN ls.usage = 'internal' AND ds.usage != 'internal' THEN -sml.qty_done
                                     WHEN ls.usage != 'internal' AND ds.usage = 'internal' THEN sml.qty_done 
                                     ELSE 0 END) + lmt_count.sdck)
                    ELSE
                        ABS(SUM(CASE WHEN ls.usage = 'internal' AND ds.usage != 'internal' THEN -sml.qty_done
                                     WHEN ls.usage != 'internal' AND ds.usage = 'internal' THEN sml.qty_done 
                                     ELSE 0 END))
                    END
                FROM
                    stock_move_line sml
                    LEFT JOIN product_product pp ON sml.product_id = pp.id
                    LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id     
                    LEFT JOIN stock_location ls ON sml.location_id = ls.id
                    LEFT JOIN stock_location ds ON sml.location_dest_id = ds.id
                    LEFT JOIN So_du_dau_ki lmt_count ON lmt_count.product_id = sml.product_id
                WHERE
                    sml.date >= '{self.date_from}' AND sml.date <= '{self.date_to}'
                GROUP BY
                        pp.id,
                    pt.name,
                        lmt_count.sdck
                ORDER BY
                        pt.name
        """
        self.env.cr.execute(luy_ke)
        lk = self.env.cr.fetchall()
        data = {
            'luy_ke': lk
        }
        return self.env.ref('om_hospital.report_month_luy_ke_xlsx').report_action(self, data=data)


    def action_print_fixlk_report(self):
        luy_ke= f"""
                WITH sd AS (
                    SELECT
                        sml.product_id,
                        SUM(CASE
                            WHEN ls.USAGE = 'internal' AND ds.USAGE != 'internal' THEN -sml.qty_done 
                            WHEN ls.USAGE != 'internal' AND ds.USAGE = 'internal' THEN sml.qty_done 
                            ELSE 0 
                        END) AS sdck 
                    FROM
                        stock_move_line sml
                        LEFT JOIN stock_location ls ON sml.location_id = ls.id
                        LEFT JOIN stock_location ds ON sml.location_dest_id = ds.id 
                    WHERE
                        sml.date <= '{self.date_from}' AND sml.state = 'done'
                    GROUP BY
                        sml.product_id
                    ORDER BY
                        sml.product_id ASC
                ) 
            
                SELECT
                    pt.name['en_US'],
                    CASE
                            WHEN sl1.USAGE = 'internal' AND sl2.USAGE != 'internal' THEN -sml.qty_done 
                            WHEN sl1.USAGE != 'internal' AND sl2.USAGE = 'internal' THEN sml.qty_done 
                            ELSE 0 
                    END AS nhap_xuat ,
                    SUM(CASE
                            WHEN sl1.USAGE = 'internal' AND sl2.USAGE != 'internal' THEN -sml.qty_done 
                            WHEN sl1.USAGE != 'internal' AND sl2.USAGE = 'internal' THEN sml.qty_done 
                            ELSE 0 
                    END ) 
                        OVER (PARTITION BY sml.product_id ORDER BY sml.date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS so_du_trong_ki,
                    CASE WHEN sd.sdck IS NOT NULL THEN sd.sdck ELSE 0 END AS so_du_truoc_ki,
                    (CASE WHEN sd.sdck IS NOT NULL THEN sd.sdck ELSE 0 END + SUM(CASE
                            WHEN sl1.USAGE = 'internal' AND sl2.USAGE != 'internal' THEN -sml.qty_done 
                            WHEN sl1.USAGE != 'internal' AND sl2.USAGE = 'internal' THEN sml.qty_done 
                            ELSE 0 
                    END ) OVER (PARTITION BY sml.product_id 
                    ORDER BY sml.date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) AS luy_ke,
                    sml.date
                FROM
                    stock_move_line sml	
                    LEFT JOIN product_product pp ON pp.id = sml.product_id
                    LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                    LEFT JOIN stock_location sl1 ON sml.location_id = sl1.id
                    LEFT JOIN stock_location sl2 ON sml.location_dest_id = sl2.id
                    LEFT JOIN sd ON sd.product_id = sml.product_id
                WHERE 
                    sml.date BETWEEN '{self.date_from}' AND '{self.date_to}' AND sml.state = 'done'
                GROUP BY
                    sml.product_id, 
                    sml.date, 
                    pt.name,sd.sdck, 
                    nhap_xuat
                ORDER BY 
                    sml.product_id DESC, 
                    sml.date ASC;
        """
        self.env.cr.execute(luy_ke)
        lk = self.env.cr.fetchall()
        data = {
            'luy_ke': lk
        }
        return self.env.ref('om_hospital.report_month_fixlk_xlsx').report_action(self, data=data)