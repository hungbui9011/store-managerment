from odoo import api, fields, models, _


class AppointmentReportWizard(models.TransientModel):
    _name = "appointment.report.wizard"
    _description = "Print Appointment Wizard"

    patient_id = fields.Many2one('hospital.patient', string="patient")
    date_from = fields.Date(string="Date From")
    date_to = fields.Date(string="Date To")

    def action_prints_excel_report(self):
        domain = []
        patient_id = self.patient_id
        if patient_id:
            domain += [('patient_id', '=', patient_id.id)]
        date_from = self.date_from
        if patient_id:
            domain += [('date_appointment', '>=', date_from)]
        date_to = self.date_to
        if patient_id:
            domain += [('date_appointment', '<=', date_to)]
        appointments = self.env['hospital.appointment'].search_read(domain)
        data = {
            'appointments': appointments,
            'form_data': self.read()[0]
        }
        return self.env.ref('om_hospital.report_patient_appointment_xlsx').report_action(self, data=data)

    def action_prints_report(self):
        domain = []
        patient_id = self.patient_id
        if patient_id:
            domain += [('patient_id', '=', patient_id.id)]
        date_from = self.date_from
        if patient_id:
            domain += [('date_appointment', '>=', date_from)]
        date_to = self.date_to
        if patient_id:
            domain += [('date_appointment', '<=', date_to)]
        appointments = self.env['hospital.appointment'].search(domain)
        appointment_list = []
        for appointment in appointments:
            vals = {
                'name': appointment.name,
                'note': appointment.note,
                'age': appointment.age,
            }
            appointment_list.append(vals)
        data = {
            'form_data': self.read()[0],
            'appointments': appointment_list
        }
        return self.env.ref('om_hospital.action_appointments_report').report_action(self, data=data)
