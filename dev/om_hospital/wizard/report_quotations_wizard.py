from odoo import api, fields, models
from odoo.sql_db import db_connect


class QuotationsReportWizard(models.TransientModel):
    _name = "report.quotations.wizard"
    _description = "Print Quotations Report Wizard"

    def action_print_report(self):
        quotations = f"""
                    SELECT
                        sale_order.name,
                        res_partner.name,
                        res_partner.phone,
                        res_partner.email,
                        res_partner.street
                    FROM
                        sale_order
                        INNER JOIN res_partner on sale_order.partner_id = res_partner.id
                    """
        self.env.cr.execute(quotations)
        quo = self.env.cr.fetchall()
        deli = {}
        quos = []
        count_go_stock = {}
        count_des_stock = {}
        count_go_qty_done = {}
        count_dest_qty_done = {}
        for quotations in quo:
            delivery = f"""
                    SELECT
                        stock_move_line.reference,
                        product_template.NAME [ 'en_US' ],
                        stock_move.product_uom_qty,
                        stock_move.quantity_done,
                        go_stock."complete_name",
                        go_stock."usage",
                        dest_stock."complete_name",
                        dest_stock."usage",
                        stock_move_line."state"
                    FROM
                        stock_move_line
                        INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                        INNER JOIN stock_location go_stock ON stock_move_line.location_id = go_stock."id" 
                        INNER JOIN stock_location dest_stock ON stock_move_line.location_dest_id = dest_stock."id"
                        INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                        INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                        INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                        INNER JOIN sale_order ON stock_picking.sale_id = sale_order."id"
                        WHERE sale_order.name = '{quotations[0]}' 
                """
            self.env.cr.execute(delivery)
            delis = self.env.cr.fetchall()

            go_count = f"""
                    SELECT
                        subquery_alias.go_stock_name,
                        subquery_alias.location_id,
                        SUM ( subquery_alias.counted_rows ) AS total_count,
                        subquery_alias.USAGE
                    FROM
                        (
                        SELECT COUNT
                            ( stock_move_line.location_id ) AS counted_rows,
                            go_stock.complete_name AS go_stock_name,
                            stock_move_line.location_id AS location_id,
                            go_stock.USAGE  
                        FROM
                            stock_move_line
                            INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                            INNER JOIN stock_location go_stock ON stock_move_line.location_id = go_stock."id"
                            INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                            INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                            INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                            INNER JOIN sale_order ON stock_picking.sale_id = sale_order."id" 
                        WHERE
                            go_stock.USAGE = 'internal' AND sale_order.name = '{quotations[0]}'
                        GROUP BY
                            stock_move_line.location_id,
                            go_stock.complete_name,
                            stock_move_line.location_id,
                            go_stock.USAGE 
                        ) AS subquery_alias 
                    GROUP BY
                        subquery_alias.go_stock_name,
                        subquery_alias.location_id,
                        subquery_alias.USAGE;
            """
            self.env.cr.execute(go_count)
            go_counts = self.env.cr.fetchall()
            count_go_stock[quotations[0]] = go_counts

            des_count = f"""
                    SELECT
                        subquery_alias.dest_stock_name,
                        subquery_alias.location_dest_id,
                        SUM ( subquery_alias.counted_rows ) AS total_count,
                        subquery_alias.USAGE
                    FROM
                        (
                        SELECT COUNT
                            ( stock_move_line.location_dest_id ) AS counted_rows,
                            dest_stock.complete_name AS dest_stock_name,
                            stock_move_line.location_dest_id AS location_dest_id,
                            dest_stock.USAGE  
                        FROM
                            stock_move_line
                            INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                            INNER JOIN stock_location dest_stock ON stock_move_line.location_dest_id = dest_stock."id"
                            INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                            INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                            INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                            INNER JOIN sale_order ON stock_picking.sale_id = sale_order."id" 
                        WHERE
                            dest_stock.USAGE = 'internal' AND sale_order.name = '{quotations[0]}'
                        GROUP BY
                            stock_move_line.location_dest_id,
                            dest_stock.complete_name,
                            stock_move_line.location_dest_id,
                            dest_stock.USAGE 
                        ) AS subquery_alias 
                    GROUP BY
                        subquery_alias.dest_stock_name,
                        subquery_alias.location_dest_id,
                        subquery_alias.USAGE;
            """
            self.env.cr.execute(des_count)
            des_counts = self.env.cr.fetchall()
            count_des_stock[quotations[0]] = des_counts

            qty_go_done_count = f"""
                    SELECT
                        product_template.NAME [ 'en_US' ] AS tensp,
                        SUM(stock_move.quantity_done) AS total_quantity_done,
                        go_stock.complete_name
                    FROM
                        stock_move_line
                        INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                        INNER JOIN stock_location go_stock ON stock_move_line.location_id = go_stock."id"
                        INNER JOIN stock_location dest_stock ON stock_move_line.location_dest_id = dest_stock."id"
                        INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                        INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                        INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                        INNER JOIN sale_order ON stock_picking.sale_id = sale_order."id"
                        WHERE go_stock.USAGE = 'internal' AND sale_order.name = '{quotations[0]}'
                    GROUP BY
                        product_template."name",
                        go_stock.complete_name,
                        stock_move.quantity_done;
            """
            self.env.cr.execute(qty_go_done_count)
            qty_go_done_counts = self.env.cr.fetchall()

            qty_dest_done_count = f"""
                    SELECT
                        product_template.NAME [ 'en_US' ] AS tensp,
                        SUM(stock_move.quantity_done) AS total_quantity_done
                    FROM
                        stock_move_line
                        INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                        INNER JOIN stock_location go_stock ON stock_move_line.location_id = go_stock."id"
                        INNER JOIN stock_location dest_stock ON stock_move_line.location_dest_id = dest_stock."id"
                        INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                        INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                        INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                        INNER JOIN sale_order ON stock_picking.sale_id = sale_order."id"
                        WHERE dest_stock.USAGE = 'internal' AND sale_order.name = '{quotations[0]}'
                    GROUP BY
                        product_template."name",
                        stock_move.quantity_done;
            """
            self.env.cr.execute(qty_dest_done_count)
            qty_dest_done_counts = self.env.cr.fetchall()
            if not delis == []:
                quos.append(quotations)
                deli[quotations[0]] = delis
                count_go_qty_done[quotations[0]] = qty_go_done_counts
                count_dest_qty_done[quotations[0]] = qty_dest_done_counts

        data = {
            'quotation': quos,
            'delivery': deli,
            'count_go_stock': count_go_stock,
            'count_des_stock': count_des_stock,
            'count_go_qty_done': count_go_qty_done,
            'count_dest_qty_done': count_dest_qty_done
        }
        return self.env.ref('om_hospital.report_quotations_details').report_action(self, data=data)

    def action_print_month_report(self):
        month = f"""
                SELECT
                    month_year
                FROM
                    (
                        SELECT
                            to_char(stock_move_line.date, 'MM-YYYY') AS month_year,
                            DATE_TRUNC('month', stock_move_line.date) AS month_trunc,
                            DATE_TRUNC('year', stock_move_line.date) AS year_trunc
                        FROM
                            stock_move_line
                        WHERE
                            to_char(stock_move_line.date, 'YYYY-MM') >= to_char((CURRENT_DATE - INTERVAL '11 MONTHS'), 'YYYY-MM')
                        GROUP BY
                            month_year, month_trunc, year_trunc
                        ORDER BY
                            year_trunc ASC,
                            month_trunc ASC
                    ) AS temp_table
                ORDER BY
                    temp_table.year_trunc ASC,
                    temp_table.month_trunc ASC;
        """
        self.env.cr.execute(month)
        mon = self.env.cr.fetchall()
        deli = {}
        mons = []
        go_qty_done = {}
        dest_qty_done = {}
        for month in mon:
            delivery = f"""
                    SELECT
                        product_template.NAME [ 'en_US' ],
                        go_stock.complete_name,
                        stock_move.product_uom_qty,
                        stock_picking.origin,
                        stock_move_line.qty_done,
                        to_char(stock_move_line.date, 'DD-MM-YYYY')
                    FROM
                        stock_move_line
                        INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                        INNER JOIN stock_location go_stock ON stock_move_line.location_id = go_stock."id"
                        INNER JOIN stock_location dest_stock ON stock_move_line.location_dest_id = dest_stock."id"
                        INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                        INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                        INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                        WHERE to_char(stock_move_line.date, 'MM-YYYY')='{month[0]}'
                    ORDER BY
                        to_char DESC;	
                """
            self.env.cr.execute(delivery)
            delis = self.env.cr.fetchall()

            exp_qty_done = f"""
                    SELECT
                        product_template.NAME [ 'en_US' ] AS product_name,
                        go_stock.complete_name AS source_location,
                        stock_move.product_uom_qty,
                        stock_picking.origin,
                        to_char(stock_move_line.date, 'DD-MM-YYYY'),
                        SUM(stock_move_line.qty_done) AS total_qty_done
                    FROM
                        stock_move_line
                        INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                        INNER JOIN stock_location go_stock ON stock_move_line.location_id = go_stock."id"
                        INNER JOIN stock_location dest_stock ON stock_move_line.location_dest_id = dest_stock."id"
                        INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                        INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                        INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                    WHERE 
                        (go_stock.usage = 'internal' OR go_stock.usage = 'transit')
                        AND to_char(stock_move_line.date, 'MM-YYYY')='{month[0]}'
                    GROUP BY
                        product_template.NAME [ 'en_US' ],
                        go_stock.complete_name,
                        dest_stock.complete_name,
                        stock_move.product_uom_qty,
                        stock_picking.origin,
                        stock_move_line.date
                    """
            self.env.cr.execute(exp_qty_done)
            gone_qty_done = self.env.cr.fetchall()
            imp_qty_done = f"""
                    SELECT
                        product_template.NAME [ 'en_US' ] AS product_name,
                        dest_stock.complete_name,
                        stock_move.product_uom_qty,
                        stock_picking.origin,
                        to_char(stock_move_line.date, 'DD-MM-YYYY'),
                        SUM(stock_move_line.qty_done) AS total_qty_done
                    FROM
                        stock_move_line
                        INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                        INNER JOIN stock_location go_stock ON stock_move_line.location_id = go_stock."id"
                        INNER JOIN stock_location dest_stock ON stock_move_line.location_dest_id = dest_stock."id"
                        INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                        INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                        INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                    WHERE 
                        (dest_stock.usage = 'internal' OR dest_stock.usage = 'transit')
                        AND to_char(stock_move_line.date, 'MM-YYYY')='{month[0]}'
                    GROUP BY
                        product_template.NAME [ 'en_US' ],
                        go_stock.complete_name,
                        dest_stock.complete_name,
                        stock_move.product_uom_qty,
                        stock_picking.origin,
                        stock_move_line.date
                                """
            self.env.cr.execute(imp_qty_done)
            impo_qty_done = self.env.cr.fetchall()

            if not delis == []:
                mons.append(month)
                deli[month[0]] = delis
                go_qty_done[month[0]] = gone_qty_done
                dest_qty_done[month[0]] = impo_qty_done

        data = {
            'month': mon,
            'delivery': deli,
            'go_qty_done': go_qty_done,
            'dest_qty_done': dest_qty_done
        }
        return self.env.ref('om_hospital.report_month_quotations_details').report_action(self, data=data)
