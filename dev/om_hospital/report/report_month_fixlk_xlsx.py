import base64
import io
import openpyxl
import json

from odoo import fields, models, api


class ReportMonthSale(models.Model):
    _name = 'report.om_hospital.report_month_fixlk_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, sale):
        sheet = workbook.add_worksheet('Báo cáo ')
        center_format = workbook.add_format({
            'align': 'center',
            'bg_color': '#00fffb',
            'bold': True,
            'border': 5})

        row = 1
        col = 1

        sheet.set_column(1, 7, 20)
        sheet.merge_range(row, col, row, col + 4, 'Báo cáo lũy kế', center_format)
        row += 1
        sheet.write(row, col, 'Tên Sản Phẩm:', center_format)
        sheet.write(row, col + 1, 'Số Lượng Nhập/Xuất:', center_format)
        sheet.write(row, col + 2, 'Số Dư Trong Kì:', center_format)
        sheet.write(row, col + 3, 'Số Dư Đầu Kì:', center_format)
        sheet.write(row, col + 4, 'Lũy Kế:', center_format)

        for luy_ke in data['luy_ke']:
            row += 1
            sheet.write(row, col, luy_ke[0])
            sheet.write(row, col + 1, luy_ke[1])
            sheet.write(row, col + 2, luy_ke[2])
            sheet.write(row, col + 3, luy_ke[3])
            sheet.write(row, col + 4, luy_ke[4])
