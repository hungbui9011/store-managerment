import base64
import io

from odoo import api, fields, models


class PatientAppointmentXlsx(models.AbstractModel):
    _name = 'report.om_hospital.report_patient_appointment_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, patients):
        sheet = workbook.add_worksheet('Appointments')
        bold = workbook.add_format({'bold': True})
        sheet.set_column(0, 10, 15)

        row = 3
        col = 3

        sheet.write(row, col, 'Reference', bold)
        sheet.write(row, col + 1, 'Patient Name', bold)
        for appointment in data['appointments']:
            row += 1
            sheet.write(row, col, appointment['name'])
            sheet.write(row, col + 1, appointment['patient_id'][1])

        # sheet = workbook.add_worksheet('Patient ID Appointment')
        # center_format = workbook.add_format({
        #     'align': 'center',
        #     'bg_color': 'yellow',
        #     'bold': True
        # })
        # left_format = workbook.add_format({
        #     'align': 'left',
        #     'bold': True
        # })
        #
        # row = 3
        # col = 3
        # sheet.set_column(0, 10, 12)
        # for obj in patients:
        #
        #     row += 1
        #     sheet.merge_range(row, col, row, col + 1, 'ID Appointment', center_format)
        #
        #     row += 1
        #     if obj.image:
        #         patient_image = io.BytesIO(base64.b64decode(obj.image))
        #         sheet.insert_image(row, col, "image.png", {'image_data':
        #                                                    patient_image, 'x_scale': 0.1,
        #                                                    'y_scale': 0.1, 'object_position': 1})
        #         row += 7
        #
        #     row += 1
        #     sheet.write(row, col, 'Name:  ', left_format)
        #     sheet.write(row, col + 1, obj.name)
        #     row += 1
        #     sheet.write(row, col, 'Age:  ', left_format)
        #     sheet.write(row, col + 1, obj.age)
        #     row += 1
        #     sheet.write(row, col, 'Reference:  ', left_format)
        #     sheet.write(row, col + 1, obj.reference)
        #     row += 1
        #     sheet.write(row, col, 'Note:  ', left_format)
        #     sheet.write(row, col + 1, obj.note)
