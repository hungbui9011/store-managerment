import base64
import io
from odoo import fields, models, api


class ReportSale(models.Model):
    _name = 'report.om_hospital.report_sale_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, sale):
        sheet = workbook.add_worksheet('Báo cáo ')
        center_format = workbook.add_format({
            'align': 'center',
            'bg_color': '#00fffb',
            'bold': True,
            'border': 5})
        row = 1
        col = 1
        sheet.set_column(0, 12, 17)
        sheet.merge_range(row, col, row, col + 12, 'Báo cáo bán hàng', center_format)
        row += 1
        sheet.write(row, col, 'Mã Vận Chuyển:', center_format)
        sheet.write(row, col + 1, 'Mã Đơn Hàng:', center_format)
        sheet.write(row, col + 2, 'Tên Người Mua:', center_format)
        sheet.write(row, col + 3, 'Tên Sản Phẩm:', center_format)
        sheet.write(row, col + 4, 'Số Lượng Đặt:', center_format)
        sheet.write(row, col + 5, 'Số Lượng Đã Giao', center_format)
        sheet.write(row, col + 6, 'Số Lượng Trả:', center_format)
        sheet.write(row, col + 7, 'Hàng Trả Đã Nhận:', center_format)
        sheet.write(row, col + 8, 'Giá Bán:', center_format)
        sheet.write(row, col + 9, 'Giá Nhập:', center_format)
        sheet.write(row, col + 10, 'Tổng Tiền Lãi:', center_format)
        sheet.write(row, col + 11, 'Tổng Tiền Lỗ:', center_format)
        for sale in data['sale']:
            if sale is None:
                sheet.write(0)
            else:
                row += 1
                sheet.write(row, col, sale[0])
                sheet.write(row, col + 1, sale[1])
                sheet.write(row, col + 2, sale[2])
                sheet.write(row, col + 3, sale[3])
                sheet.write(row, col + 4, sale[4])
                sheet.write(row, col + 5, sale[5])
                sheet.write(row, col + 6, sale[6])
                sheet.write(row, col + 7, sale[7])
                sheet.write(row, col + 8, sale[8])
                sheet.write(row, col + 9, sale[9])
                sheet.write(row, col + 10, sale[10])
                sheet.write(row, col + 11, sale[11])
