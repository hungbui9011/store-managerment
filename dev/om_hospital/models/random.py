import random
import string


def generate_random_phone_number():
    area_code = random.choice(['01', '02', '03', '04', '05', '06', '07', '08', '09'])
    first_three_digits = str(random.randint(100, 999))
    last_four_digits = str(random.randint(1000, 9999))
    return f"({area_code}) {first_three_digits}-{last_four_digits}"

def generate_random_email():
    letters = string.ascii_lowercase
    username = ''.join(random.choice(letters) for i in range(6))
    domain = ''.join(random.choice(letters) for i in range(4))
    email = f"{username}@{domain}.com"
    return email
