from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from .random import generate_random_phone_number,generate_random_email


class HospitalPatient(models.Model):
    _name = "hospital.patient"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Hospital Patient"
    _order = "id desc"

    @api.model
    def default_get(self, fields):
        res = super(HospitalPatient, self).default_get(fields)
        res['note'] = 'NEW Patient Creating!'
        return res

    name = fields.Char(string='Tên', required=True, tracking=True)
    reference = fields.Char(string="Sequence", requied=True, copy=False, readonly=True,
                            default=lambda self: _('New'))
    age = fields.Integer(string='Age', tracking=True)
    phone = fields.Char(string='Phone', tracking=True)
    email = fields.Char(string='Email', tracking=True)
    gender = fields.Selection([
        ('male', 'Nam'),
        ('female', 'Nữ'),
        ('other', 'Khác'),
    ], required=True, default='male', tracking=True)
    note = fields.Text(string="Description")
    state = fields.Selection([
        ('draft', 'Draft'), ('confirm', 'Confirmed'),
        ('done', 'Done'), ('cancel', 'Canceled')
    ], default='done', string="Status", tracking=True)
    responsible_id = fields.Many2one('res.partner', string="Responsible")
    appointment_count = fields.Integer(string='Appointment Count', compute='_compute_appointment_count')
    image = fields.Binary(string="Patient Image")
    appointment_ids = fields.One2many('hospital.appointment', 'patient_id', string="Appointments")

    def _compute_appointment_count(self):
        for rec in self:
            appointment_count = self.env['hospital.appointment'].search_count([('patient_id', '=', rec.id)])
            rec.appointment_count = appointment_count

    def action_confirm(self):
        for rec in self:
            rec.state = 'confirm'

    def action_done(self):
        for rec in self:
            rec.state = 'done'

    def action_draft(self):
        for rec in self:
            rec.state = 'draft'

    def action_cancel(self):
        for rec in self:
            rec.state = 'cancel'

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'New patient'
        if vals.get('reference', _('New') == _('New')):
            vals['reference'] = self.env['ir.sequence'].next_by_code('hospital.patient') or _('New')
        res = super(HospitalPatient, self).create(vals)
        if 'phone' not in res or not res['phone']:
            res['phone'] = generate_random_phone_number()
        if 'email' not in res or not res['email']:
            res['email'] = generate_random_email()
        return res

    # @api.model
    # def default_get(self, values):
    #     res = super(HospitalPatient, self).default_get(values)
    #     res['gender'] = 'male'
    #     res['age'] = '20'
    #     return res

    @api.constrains('name')
    def check_name(self):
        for rec in self:
            patients = self.env['hospital.patient'].search([('name', '=', rec.name), ('id', '!=', rec.id)])
            if patients:
                raise ValidationError(_("Name %s Alreadly Exisis in Patients" % rec.name))

    @api.constrains('age')
    def check_age(self):
        for rec in self:
            if rec.age == 0:
                raise ValidationError(_("Age cannot be 0!"))

    def name_get(self):
        res = []
        for rec in self:
            if not self.env.context.get('hide_code'):
                name = '[' + rec.reference + ']' + rec.name
            else:
                name = rec.name
            res.append((rec.id, name))
        return res

    def _get_report_base_filename(self):
        return self.name

    def action_open_appointments(self):
        return {
            'name': 'Appointments',
            'type': 'ir.actions.act_window',
            'res_model': 'hospital.appointment',
            'domain': [('patient_id', '=', self.id)],
            'context': {'default_patient_id': self.id},
            'view_mode': 'tree,form',
            'target': 'current',
        }

    # @api.model
    # def create(self, vals):
    #     if 'phone' not in vals or not vals['phone']:
    #         vals['phone'] = generate_random_phone_number()
    #     return super(HospitalPatient, self).create(vals)
