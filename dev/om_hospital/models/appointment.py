from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class HospitalAppointment(models.Model):
    _name = "hospital.appointment"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Hospital Appointment"
    _order = "write_date desc"

    name = fields.Char(string="Name", requied=True, copy=False, readonly=True,
                       default=lambda self: _('New'))
    patient_id = fields.Many2one('hospital.patient', string="Patient", required=True)
    patient_name_id = fields.Many2one('hospital.patient', string="Patient Name", required=True)
    doctor_id = fields.Many2one('hospital.doctor', string="Doctor")
    age = fields.Integer(string='Age', related='patient_id.age')
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other'),
    ],  tracking=True)
    state = fields.Selection([
        ('draft', 'Draft'), ('confirm', 'Confirmed'),
        ('done', 'Done'), ('cancel', 'Canceled')
    ], default='done', string="Status", tracking=True)
    note = fields.Text(string="Description")
    date_appointment = fields.Date(string="Date")
    date_checkup = fields.Datetime(string="Check up Time")
    prescription = fields.Text(string="Prescription")
    prescription_line_ids = fields.One2many('appointment.prescription.line', 'appointment_id',
                                            string='Prescription Lines')

    def action_confirm(self):
        self.state = 'confirm'

    def action_done(self):
        self.state = 'done'

    def action_draft(self):
        self.state = 'draft'

    def action_cancel(self):
        self.state = 'cancel'

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'New patient'
        if vals.get('name', _('New') == _('New')):
            vals['name'] = self.env['ir.sequence'].next_by_code('hospital.appointment') or _('New')
        res = super(HospitalAppointment, self).create(vals)
        return res

    @api.onchange('patient_id')
    def onchange_patient_id(self):
        if self.patient_id:
            if self.patient_id.gender:
                self.gender = self.patient_id.gender
        else:
            self.gender = ''

    def unlink(self):
        if self.state == 'done':
            raise ValidationError("You can not delete this %s as it is in Done State" % self.name)
        res = super(HospitalAppointment, self).unlink()
        return res

    def action_url(self):
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': 'https://poe.com',
        }


class AppointmentPrescription(models.Model):
    _name = "appointment.prescription.line"
    _description = "Appointment Prescription Lines"

    name = fields.Char(string="Medicine")
    qty = fields.Integer(string="Quantity")
    appointment_id = fields.Many2one('hospital.appointment')