from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class StoreProducts(models.Model):
    _name = 'store.products'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Product Description'

    name = fields.Char(string='Tên Sản Phẩm', required=True, tracking=True)
    note = fields.Text(string="Mô Tả Sản Phẩm")
    image = fields.Binary(string="Hình ảnh mô tả")
    price = fields.Integer(string='Giá Gốc', tracking=True, copy=False)
    discount_percent = fields.Integer(string='Giảm giá (%)', tracking=True, copy=False)
    discount_price = fields.Float(string="Giá Sản Phẩm Hiện Tại", compute='_compute_discount_percentage')
    create_date = fields.Datetime(string='Ngày Tạo')
    category_id = fields.Many2one('store.category', string='Category')
    reference = fields.Char(string="Order Reference", requied=True, copy=False, readonly=True,
                            default=lambda self: _('New'))
    state = fields.Selection([
        ('stocking', 'Còn Hàng'), ('out_of_stock', 'Hết Hàng')
    ], default='stocking', string="Trạng Thái", tracking=True)

    def unlink(self):
        if self.state == 'stocking':
            raise ValidationError("Bạn không thể xóa sản phẩm %s khi sản phẩm vẫn đang còn hàng!" % self.name)
        res = super(StoreProducts, self).unlink()
        return res

    def action_stocking(self):
        for rec in self:
            rec.state = 'stocking'

    def action_out_of_stock(self):
        for rec in self:
            rec.state = 'out_of_stock'

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'Sản Phẩm Mới'
        if vals.get('reference', _('New') == _('New')):
            vals['reference'] = self.env['ir.sequence'].next_by_code('store.products') or _('New')
        res = super(StoreProducts, self).create(vals)
        return res

    @api.constrains('name')
    def check_name(self):
        for rec in self:
            patients = self.env['store.products'].search([('name', '=', rec.name), ('id', '!=', rec.id)])
            if patients:
                raise ValidationError(_("Sản phẩm %s đã được nhập! Vui lòng nhập sản phẩm không trùng lặp" % rec.name))

    @api.constrains('price')
    def check_age(self):
        for rec in self:
            if rec.price == 0:
                raise ValidationError(_("Giá sản phẩm không thể bằng 0!"))

    def _compute_discount_percentage(self):
        for rec in self:
            if rec.discount_percent != 0:
                rec.discount_price = rec.price - (rec.price * (rec.discount_percent / 100))
            else:
                rec.discount_price = rec.price
