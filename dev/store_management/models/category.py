from odoo import fields, models, api, _


class StoreCategory(models.Model):
    _name = 'store.category'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Category Description'

    name = fields.Char(string='Tên Danh Mục', required=True, tracking=True)
    note = fields.Text(string="Mô Tả Danh Mục")
    image = fields.Binary(string="Hình Ảnh Minh Họa")
    products_ids = fields.One2many('store.products', 'category_id', string='Sản Phẩm')
    products_count = fields.Integer(string='Số lượng sản phẩm', compute='_compute_products_count')
    reference = fields.Char(string="Order Reference", requied=True, copy=False, readonly=True,
                            default=lambda self: _('New'))

    def _compute_products_count(self):
        for rec in self:
            products_count = self.env['store.products'].search_count([('category_id', '=', rec.id)])
            rec.products_count = products_count

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'Loại Sản Phẩm Mới'
        if vals.get('reference', _('New') == _('New')):
            vals['reference'] = self.env['ir.sequence'].next_by_code('store.category') or _('New')
        res = super(StoreCategory, self).create(vals)
        return res

    def action_open_products(self):
        return {
            'name': 'Sản Phẩm',
            'type': 'ir.actions.act_window',
            'res_model': 'store.products',
            'domain': [('category_id', '=', self.id)],
            'context': {'default_category_id': self.id},
            'view_mode': 'kanban,tree,form',
            'target': 'current',
        }
