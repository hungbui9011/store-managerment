{
    'name': 'Quản lý cửa hàng',
    'version': '14.0.1.0.0',
    'summary': 'Store Managerment Software',
    'sequence': -100,
    'description': """ Quản lý cửa hàng """,
    'depends': ['mail', ],
    # data: security>data>view>report
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'wizard/create_products_view.xml',
        'wizard/search_products.xml',
        'wizard/all_product_report_view.xml',
        'views/menu.xml',
        'views/products.xml',
        'views/category.xml',
        'report/report.xml',
        'report/products_card.xml',
        'report/all_products_list.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
