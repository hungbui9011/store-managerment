from odoo import api, fields, models, _


class ProductReportWizard(models.TransientModel):
    _name = "products.report.wizard"
    _description = "Print Products Wizard"

    category_id = fields.Many2one('store.category', string='Loại Hàng Muốn Tìm')
    discount_percent = fields.Float(string="% Giảm Giá Muốn Tìm")

    def action_print_report(self):
        domain = []
        category_id = self.category_id
        if category_id:
            domain += [('category_id', '=', category_id.id)]
        discount_percent = self.discount_percent
        if discount_percent:
            domain += [('discount_percent'), '=', discount_percent]
        product = self.env['store.products'].search(domain)
        product_list = []
        for products in product:
            vals = {
                'name': products.name,
                'price': products.price,
                'note': products.note,
            }
            product_list.append(vals)
        data = {
            'form_data': self.read()[0],
            'product': product_list
        }
        return self.env.ref('store_management.action_report_all_products_details').report_action(self, data=data)
