from odoo import api, fields, models, _


class SearchProductsWizard(models.TransientModel):
    _name = "search.products.wizard"
    _description = "Search Products Wizard"

    category_id = fields.Many2one('store.category', string="Tên Danh Mục", requied=True)

    def action_search_products(self):
        return {
            'name': 'Sản Phẩm Có Trong Danh Mục',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,tree,form',
            'view_type': 'form',
            'res_model': 'store.products',
            'domain': [('category_id', '=', self.category_id.id)],
            'target': 'current',
        }
        return action
