from odoo import fields, models, api, _
from odoo.exceptions import ValidationError

class CreateProductsWizard(models.Model):
    _name = 'create.products.wizard'
    _description = 'Thêm mới sản phẩm'

    @api.model
    def default_get(self, fields):
        res = super(CreateProductsWizard, self).default_get(fields)
        res['category_id'] = self._context.get('active_id')
        return res

    name = fields.Char(string='Tên Sản Phẩm', required=True, tracking=True)
    note = fields.Text(string="Mô Tả")
    image = fields.Binary(string="Hình Ảnh Minh Họa")
    price = fields.Integer(string='Giá Sản Phẩm', tracking=True, copy=False)
    category_id = fields.Many2one('store.category', string="Danh Mục Sản Phẩm", required=True)
    discount_percent = fields.Float(string='Giảm giá (%)', tracking=True, copy=False)
    discount_price = fields.Float(string="Giá Sản Phẩm Hiện Tại", compute='_compute_discount_percentage')

    def create_products_wizard(self):
        vals = {
            'name': self.name,
            'note': self.note,
            'image': self.image,
            'price': self.price,
            'discount_percent': self.discount_percent,
            'discount_price': self.discount_price,
            'category_id': self.category_id.id,

        }
        products_rec = self.env['store.products'].create(vals)
        return {
            'name': _('Products'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'store.products',
            'res_id': products_rec.id,
            'target': 'new'
        }

    def view_products_wizard(self):
        # method 1
        view = self.env.ref('store_management.action_store_products').read()[0]
        view['domain'] = [('category_id', '=', self.category_id.id)]
        return view

    @api.constrains('name')
    def check_name(self):
        for rec in self:
            patients = self.env['store.products'].search([('name', '=', rec.name), ('id', '!=', rec.id)])
            if patients:
                raise ValidationError(_("Sản phẩm %s đã được nhập! Vui lòng nhập sản phẩm không trùng lặp" % rec.name))

    @api.constrains('price')
    def check_age(self):
        for rec in self:
            if rec.price == 0:
                raise ValidationError(_("Giá sản phẩm không thể bằng 0!"))

    def _compute_discount_percentage(self):
        for rec in self:
            if rec.discount_percent != 0:
                rec.discount_price = rec.price - (rec.price * (rec.discount_percent / 100))
            else:
                rec.discount_price = rec.price
